from flask import Flask, render_template, jsonify, request, redirect, flash, url_for
import os
import json
import os.path
from pymongo import MongoClient
import re
import pandas as pd

app = Flask(__name__)

##mongo connection
client = MongoClient("54.148.142.9",22017)
db = client.db_investify
collection = db.collection_investify
collection_corpus = db.collection_investify_corpus

# client = MongoClient("localhost",27017)
# db = client.test_database
# collection = db.test_collection1
# collection_corpus = db.test_collection_corpus


def find_files_mongo():
	cursor=collection.find()
	file_list=[]
	for doc in cursor:
		file_list.append(doc['pdf-meta']['name'])
	return file_list


@app.route("/")
def index():
    path_to_json = 'files/'
    json_files = find_files_mongo()

    cursor=collection.find()
    for doc in cursor:
        if (doc['pdf-meta']['name']) == json_files[0]:
            final_data = doc
            pdf_meta = final_data['pdf-meta']

            file_name = pdf_meta['name']
            date = pdf_meta['creation_date']
            k = list()
            for gg,hh in final_data['data'].items():
                pp = {}
                pp['filename'] = file_name
                pp['metric'] = gg
                pp['date'] = date
                pp['value'] = hh
                k.append(pp)

            return render_template('template.html', my_string="Flask App", my_list=k, files=json_files, filename=file_name)

@app.route("/loaddata", methods=['GET','POST'])
def loaddata():
    data = request.get_json()
    filename = data['filename']
    json_files = find_files_mongo()
    cursor=collection.find()
    
    for doc in cursor:
    	if (doc['pdf-meta']['name']) == filename:
    		final_data = doc
    		pdf_meta = final_data['pdf-meta']
    		file_name = pdf_meta['name']
    		date = pdf_meta['creation_date']
    		k = list()
    		for gg,hh in final_data['data'].items():
    			pp = {}
    			pp['filename'] = file_name
    			pp['metric'] = gg
    			pp['date'] = date
    			pp['value'] = hh
    			k.append(pp)
    		return render_template('content.html',  my_list=k)


@app.route("/updatejson", methods=['POST'])
def updatejson():
	request_data = request.form
	request_data = request_data.to_dict()
	cursor=collection.find()

	for doc in cursor:
		if (doc['pdf-meta']['name']) == request_data['filename']:
			for key in request_data.keys():
				if key != 'filename':
					if request_data[key] != '':
						met, pg_no, idx = key.split('_')
						print(met, pg_no, idx)
						query_var = {"pdf-meta.name": request_data['filename']}
						newvalues = { "$set": { "data." + str(met) + "." + str(pg_no) + "." + str(int(idx) - 1) + '.value': request_data[key] }}
						mongo_res = collection.update_one(query_var, newvalues)

	return redirect("/")


@app.route('/upload_corpus', methods=['GET','POST'])
def read_from_json():
    if request.method == 'POST':
        document = request.files["document"]
        if not document:
            flash('Please select a File.')
            return render_template('upload_corpus.html')
        try:
            df = pd.read_csv(document,sep= "\t")
        except:
            flash('Select valid file')
            return render_template('upload_corpus.html')

        df = df.to_dict("r")
        for metric in df:
            metric['values'] = metric['values'].split(', ')
        
        collection_corpus.delete_many({})
        final_corpus = {}
        final_corpus['corpus'] = df
        collection_corpus.insert_one(final_corpus)
        print(final_corpus)
        print('success')
        return redirect('/')
    
    return render_template('upload_corpus.html')


if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.debug = True
    # app.run()
    app.run(host='0.0.0.0')