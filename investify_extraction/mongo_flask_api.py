import json
from pymongo import MongoClient
from flask import Flask, jsonify, request

app = Flask(__name__)

client = MongoClient("54.148.142.9",22017)
db = client.db_investify
collection = db.collection_investify

# @app.route('/upload/<string:file_name>', methods=['GET'])
# def load_json(file_name):
# 	with open(file_name) as f:
# 		file_data = json.load(f)
# 	collection.insert_one(file_data)
# 	client.close()
# 	return "Success"

# @app.route('/retrieve_query/<string:document>', methods=['GET'])
# def read_from_query(document):
# 	retrieved_collection = collection.find_one({"pdf_meta.document_id": document},{"_id": 0})
# 	client.close()
# 	return retrieved_collection
	
# @app.route('/retrieve_form', methods=['GET','POST'])
# def read_from_form():
# 	if request.method == 'POST':
# 		document = request.form['document']
# 		retrieved_collection = collection.find_one({"pdf_meta.document_id": document},{"_id": 0})
# 		if retrieved_collection is None:
# 			return '''The document {} does not exist'''.format(document)
# 		return retrieved_collection
			
# 	return '''<form method="POST">
# 	Document Name<input type="text" name="document">
# 	<input type="submit">
# 	</form>'''

@app.route('/retrieve_json', methods=['POST'])
def read_from_json():
	if request.method == 'POST':
		document = request.get_json()["document"]
		retrieved_collection = collection.find_one({"pdf-meta.name": document},{"_id": 0})
		#retrieved_collection = collection.find_one({"pdf_meta.document_id": document},{"_id": 0})
		if retrieved_collection is None:
			return '''The document {} does not exist'''.format(document)
		return retrieved_collection

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
