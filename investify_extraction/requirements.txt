absl-py==0.9.0
appdirs==1.4.3
apturl==0.5.2
asn1crypto==0.24.0
astor==0.8.1
attrs==19.3.0
backcall==0.1.0
beautifulsoup4==4.8.2
bleach==3.1.1
Brlapi==0.6.6
certifi==2019.11.28
chardet==3.0.4
command-not-found==0.3
cryptography==2.1.4
cupshelpers==1.0
cycler==0.10.0
decorator==4.4.1
defer==1.0.6
defusedxml==0.6.0
demjson==2.2.4
distlib==0.3.0
distro-info===0.18ubuntu0.18.04.1
entrypoints==0.3
filelock==3.0.12
fuzzywuzzy==0.18.0
gast==0.3.3
google-pasta==0.1.8
grpcio==1.27.2
h5py==2.10.0
httplib2==0.9.2
idna==2.9
importlib-metadata==1.5.0
importlib-resources==1.0.2
imutils==0.5.3
ipython==7.12.0
ipython-genutils==0.2.0
jedi==0.16.0
Jinja2==2.11.1
jsonschema==3.2.0
jtutils==0.0.6
jupyter-core==4.6.3
Keras==2.3.1
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.0
keyring==10.6.0
keyrings.alt==3.0
kiwisolver==1.1.0
language-selector==0.1
launchpadlib==1.10.6
lazr.restfulclient==0.13.5
lazr.uri==1.0.3
leven==1.0.4
louis==3.5.0
macaroonbakery==1.1.3
Mako==1.0.7
Markdown==3.2.1
MarkupSafe==1.1.1
matplotlib==3.2.0
minecart==0.3.0
mistune==0.8.4
more-itertools==8.1.0
nbconvert==5.6.1
nbformat==5.0.4
netifaces==0.10.4
nltk==3.4.5
nose==1.3.7
numpy==1.18.1
oauth==1.0.1
olefile==0.45.1
opencv-python==4.1.2.30
packaging==20.0
pandas==1.0.2
pandocfilters==1.4.2
parso==0.6.1
pdf2image==1.12.1
pdfminer3k==1.3.1
pexpect==4.8.0
pickleshare==0.7.5
Pillow==7.0.0
pluggy==0.13.1
ply==3.11
prompt-toolkit==3.0.3
protobuf==3.11.3
ptyprocess==0.6.0
py==1.8.1
pycairo==1.16.2
pycrypto==2.6.1
pycups==1.9.73
Pygments==2.5.2
pygobject==3.26.1
pymacaroons==0.13.0
PyNaCl==1.1.2
pyparsing==2.4.6
pyPdf==1.13
PyPDF2==1.26.0
pyRFC3339==1.0
pyrsistent==0.15.7
pytesseract==0.3.1
pytest==5.3.2
python-apt==1.6.5+ubuntu0.2
python-csv==0.0.11
python-dateutil==2.8.1
python-debian==0.1.32
pytz==2019.3
pyxdg==0.25
PyYAML==5.3.1
reportlab==3.4.0
requests==2.23.0
requests-unixsocket==0.1.5
scipy==1.4.1
SecretStorage==2.3.1
selenium==3.141.0
simplejson==3.13.2
six==1.14.0
soupsieve==2.0
ssh-import-id==5.7
system-service==0.3
systemd-python==234
tensorboard==1.14.0
tensorflow==1.14.0
tensorflow-estimator==1.14.0
termcolor==1.1.0
testpath==0.4.4
traitlets==4.3.3
ubuntu-drivers-common==0.0.0
ufw==0.36
unattended-upgrades==0.1
urllib3==1.25.8
usb-creator==0.3.3
virtualenv==20.0.5
wadllib==1.3.2
wcwidth==0.1.8
webencodings==0.5.1
Werkzeug==1.0.0
wrapt==1.12.0
xkit==0.0.0
xlrd==1.2.0
nltk==3.5
xmltodict==0.12.0
zipp==3.0.0
zope.interface==4.3.2
