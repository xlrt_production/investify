import argparse
import keras
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
import glob
import numpy as np
import itertools
import shutil
from utils import *
import Paragraph_to_Dictionary
import ast
import math
import copy
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import json
import os
from pymongo import MongoClient
import pandas as pd
import yaml
import boto3
import botocore
import nltk
from nltk.util import ngrams
from nltk.corpus import stopwords
from datetime import datetime,date
import re

#argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--csv_path', type=str, default=None, help="path of the excel file")
args = parser.parse_args()


##parsing yaml file to fetch the paths
paths_yaml_file = open("./paths.yaml")
parsed_yaml_file = yaml.load(paths_yaml_file, Loader=yaml.FullLoader)

##Model Path
model_path = parsed_yaml_file['paths']['model-path']
retina_model = models.load_model(model_path, backbone_name='resnet101')
labels_to_names = {0: 'box', 1: 'table', 2:'column', 3:'header'}

pdf_images_dir = parsed_yaml_file['paths']['pdf_images_dir']
output_dir = parsed_yaml_file['paths']['output_dir']

# mongo connection
client = MongoClient("54.148.142.9",22017)
db = client.db_investify
collection = db.collection_investify
collection_corpus = db.collection_investify_corpus

# client = MongoClient("localhost",27017)
# db = client.test_database
# collection = db.test_collection1
# collection_corpus = db.test_collection_corpus


def get_table_header(pdf_images_dir):
    image_dir = glob.glob(pdf_images_dir+'*Temp.jpg')
    retina_array = []
    table_header_coord = []
    resize_pdf_images(image_dir)
    for images in image_dir:
        retina_image = read_image_bgr(images)
        retina_image = preprocess_image(retina_image)
        retina_image, scale = resize_image(retina_image, min_side=1200, max_side=2000)
        retina_array.append(retina_image)

    retina_array = np.asarray(retina_array)
    boxes, scores, labels = retina_model.predict_on_batch(retina_array)
    boxes /= scale
    boundary_buffer = 2

    for i in range(len(image_dir)):
        header_list, table_list ,line_item_list,para_cells,col_list= [],[],[],[],[] #change
        image_cv = cv2.imread(image_dir[i])
        img_h, img_w = image_cv.shape[:2]
        for box, score, label in zip(boxes[i], scores[i], labels[i]):
            if score < 0.5:
                break
            b = box.astype(int)
            x1, y1, x2, y2 = b[0], b[1], b[2], b[3]
            x1 = 0 if x1 < 0 else (x1 + boundary_buffer)
            y1 = 0 if y1 < 0 else (y1 + boundary_buffer)
            x2 = img_w if x2 > img_w else (x2 - boundary_buffer)
            y2 = img_h if y2 > img_h else (y2 - boundary_buffer)
            #changes
            if label == 1:
                table_list.append([x1, y1, x2, y2])
            if label == 3:
                header_list.append([x1, y1, x2, y2])
            if label == 0:
                line_item_list.append([x1, y1, x2, y2])
            if label == 2:
                col_list.append([x1, y1, x2, y2])

        col_list = [col for col in col_list if abs(col[0]-col[2]) > img_w /13 ]

        if table_list:
            for table in table_list:
                table_box = gen_rectangle_box(table[0], table[1], table[2], table[3])
                   
                for data in line_item_list:
                    data_box = gen_rectangle_box(data[0], data[1], data[2], data[3])
                    iou = get_iou(table_box, data_box, mod=True)
                    if iou > 0.6:
                        continue
                    else:
                        para_cells.append(data) 
        else:
            para_cells=line_item_list

        #changes
        table_list_modified = []
        if table_list:
            for table in table_list:
                col_counter = 0
                table_box = gen_rectangle_box(table[0], table[1], table[2], table[3])
                table_col = []  
                for col in col_list:
                    col_box = gen_rectangle_box(col[0], col[1], col[2], col[3])
                    iou = get_iou(table_box, col_box, mod=True)
                    if iou > 0.6: #0.5
                        col_counter = col_counter +1
                        table_col.append(col)
                if col_counter > 2:
                    table_list_modified.append(table)
                
        table_list = table_list_modified

        if len(table_list) < 1 and len(header_list) < 1:
            table_header_coord.append([[], []])
        if len(table_list) > 0:
        	table_coord = non_max_suppression(np.asarray(table_list)).tolist()
        else:
        	table_coord = []
        if len(header_list) > 0:
        	header_coord = non_max_suppression(np.asarray(header_list)).tolist()
        else:
        	header_coord = []
        if len(col_list) > 0:
            col_coord = non_max_suppression(np.asarray(col_list)).tolist()
        else:
            col_coord = []

        table_header_coord.append([table_coord, header_coord, image_dir[i],para_cells,col_coord])

    return table_header_coord


def gen(paragraph_data:dict, temp_corpus) -> None:
    keyword_dict = dict()
    stop_words = set(stopwords.words('english'))
    
    # iterate the corpus
    for domain, contexts in temp_corpus.items():
        paragraph_dict = dict()
        temp_count = 0
        
        # check if domain exist into the keys
        if domain not in keyword_dict.keys():
            keyword_dict[domain] = dict()

        # iterate the extraction
        for key, values in paragraph_data.items():
            page = key[key.index("Page"):].replace("Page", "")
            if page not in keyword_dict[domain].keys():
                keyword_dict[domain][page] = list()
                
            # try:
            #     fl_name = key.rsplit('_',1)[0]+'.pdf'    
            # except Exception:
            #     fl_name = key.rsplit('_',1)[0]+'.PDF'
                
            for text in values:
                if domain not in contexts:
                    contexts.append(domain)
                
                for context in contexts:
                    #num=len(context.split())
                    num = len(re.findall(r"[\w']+", context))
                    nltk_tokens = nltk.word_tokenize(text["value"])
                    filtered_tokens = [w for w in nltk_tokens if not w in stop_words]
                    # final_tokens = [x[0] for x in nltk.pos_tag(filtered_tokens) if x[1] in ('NN','NNP')]
                    # for each_split in ngrams(final_tokens, num):
                    for each_split in ngrams(filtered_tokens, num):
                        if len(each_split) < num:
                            continue
                        if any(x[1] not in 'NN*' for x in nltk.pos_tag(each_split)):
                            continue
                        word = ' '.join(each_split)
                        ratio = fuzz.token_sort_ratio(word, context)
                        if ratio >= 85:
                            break
                    else:
                        continue
                    break
                else:
                    continue
                temp_count += 1
                text['paragraph']=int(temp_count)
                keyword_dict[domain][page].append(text)
                
    return keyword_dict


def remove_new_line (paragraph_data):
    for key in paragraph_data.keys():
        for val in paragraph_data[key]:
            val['value']=val['value'].replace('\n',' ')

    return  paragraph_data


def download_from_s3(each):
    BUCKET_NAME = 'investfy' # replace with your bucket name
    s3 = boto3.resource('s3',aws_access_key_id='AKIARHCA43B5X3RVLU5X',aws_secret_access_key='FLE/qoWvDW1Osf9eter8waJniUCFw2gnjDru54Zw',region_name='us-west-2')
    buck=s3.Bucket(BUCKET_NAME)
    dr=buck.Object(r'upload/'+each)
    if not os.path.exists('./master_data/uploads/'):
        os.makedirs('./master_data/uploads/')
    dr.download_file('master_data/uploads/'+each)
    

def move_to_archived(each):
    s3 = boto3.resource('s3',aws_access_key_id='AKIARHCA43B5X3RVLU5X',aws_secret_access_key='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',region_name='us-west-2')
    copy_source = {
                    'Bucket': 'investfy',
                    'Key': 'upload/'+each
    }
    s3.meta.client.copy(copy_source, 'investfy', 'archived/'+each)
    s3.Object('investfy', 'upload/'+each).delete()



if __name__ == "__main__":

    #reading excel file using arg parse
    df = pd.read_csv(args.csv_path)

    cursor = collection_corpus.find_one({},{"_id": 0})
    temp_corpus = {}

    for x in cursor['corpus']:
        temp_corpus[x['metric']] = x['values']
    print('temp_corpus == ',temp_corpus)

    inp_json = df.to_dict('r')
    print('input_json ',inp_json)
    for each in inp_json:
        file_name = each["PDF_FileName"]
        page_from = each["Page From"]
        page_to = each["Page To"]
        page_num = list(range(int(page_from), int(page_to) + 1))
        
        # download files from 's3'
        download_from_s3(file_name)
        pdf_file = parsed_yaml_file['paths']['pdf-path']+'uploads/'+file_name 

        print(f'pdf_file = {pdf_file}')
        print(f'page_num = {page_num}')
        
        if os.path.exists(output_dir):
            shutil.rmtree(output_dir)
        os.makedirs(output_dir)
        
        pdf_name = pdf_file[(pdf_file.rfind('/') + 1) : -4]
        final_output_path = output_dir + pdf_name + '/'
        if not os.path.exists(final_output_path):
            os.makedirs(final_output_path)

        ##Get the images out of pdf
        get_pdf_images(pdf_images_dir, pdf_file, page_num)
        # Get the table and header coordinates
        table_header_coord = get_table_header(pdf_images_dir)

        #coordinate generate
        para_dict={'data':[]}
        for table_header in table_header_coord: #for each page
            if len(table_header) != 5:
                continue
            table_coord_list = table_header[0]
            header_coord_list = table_header[1]
            col_coord_list = table_header[-1]
            orig_image_path = table_header[2].replace('Temp.jpg', '.jpg')
            image_name = orig_image_path[(orig_image_path.rfind('/') + 1) : ]

            para_dict['data'].append({'page_name':image_name,'para_cells':table_header[3]})

            ##Copying the file from 'pdf_images' directory to 'Output' directory
            shutil.copy(orig_image_path, final_output_path + image_name)
            image_cv = cv2.imread(final_output_path + image_name)
            image_box_all = cv2.imread(final_output_path + image_name)

            ##Work with header and table coord here
            padding_x = 100
            padding_y = 50

            im_name=image_name.split('.')[0]

            for (header_coord, table_coord,col_coord) in itertools.zip_longest(header_coord_list,table_coord_list, col_coord_list):
                
                if header_coord is not None:
                    cv2.rectangle(image_cv, (header_coord[0]  , header_coord[1] ),
                                  (header_coord[2]  , header_coord[3]), (255, 255, 255), -1)
                    cv2.rectangle(image_box_all, (header_coord[0]  , header_coord[1] ),
                                  (header_coord[2], header_coord[3] ), (0, 255, 0), 2) 

                if table_coord is not None:
                    cv2.rectangle(image_cv, (table_coord[0] + padding_x, table_coord[1] ),
                                  (table_coord[2]  - padding_x , table_coord[3]), (255, 255, 255), -1)
                    cv2.rectangle(image_box_all, (table_coord[0] +padding_x , table_coord[1] ),
                                  (table_coord[2] - padding_x, table_coord[3] ), (0, 0, 255), 2)

                    cv2.putText(image_box_all, str((table_coord[0],table_coord[1])),(table_coord[0],table_coord[1]),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 30), 3, cv2.LINE_AA)

                if col_coord is not None:
                    cv2.rectangle(image_box_all, (col_coord[0] , col_coord[1] ),
                                  (col_coord[2] , col_coord[3] ), (255, 0, 0), 2)
                    cv2.putText(image_box_all, str((col_coord[0],col_coord[1])),(col_coord[0], col_coord[1]),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 30), 3, cv2.LINE_AA)

            cv2.imwrite(final_output_path + image_name, image_cv)
            
            if not os.path.exists('./debug'):
                os.makedirs('./debug')
            cv2.imwrite('./debug/'+ image_name, image_box_all)

    
        #json generation for paragraph
        paragraph_data =Paragraph_to_Dictionary.get_paragraph(file_name,para_dict)
        paragraph_data=remove_new_line(paragraph_data)

        #extracted paragraph saving
        if not os.path.exists('./paragraph'):
            os.makedirs('./paragraph')
        o_file1 = './paragraph/' + pdf_file[(pdf_file.rfind('/') + 1) : -4] + '_'+str(page_num)+'par.json'
        output_file1 = open(o_file1, "w")
        json.dump(paragraph_data, output_file1, indent = 4)

        final_dict = gen(paragraph_data,temp_corpus)    
        
        #appending final_dict into data of corpus_json
        corpus_json={'pdf-meta':{"name":"","author":"","modified_date":"","creation_date":"","pages":""},
                    'data':final_dict}
        
        meta_name=pdf_file.split('/',-1)[-1]
        meta_name=meta_name.split('.')[0]
        corpus_json['pdf-meta']['name']=meta_name
        corpus_json['pdf-meta']['pages']=str(page_num)

        ##finding current timestamp in hh:mm:ss format
        try:
            today = date.today()
            now = datetime.now()
            current_time = now.strftime("%H:%M:%S")
            today = today.strftime("%d-%m-%Y")
            date_time_now = (today)+' '+current_time
            corpus_json['pdf-meta']['creation_date'] = date_time_now
        except Exception:
            print('DateTime Error')

        print('corpus_json ',corpus_json)
        #saving json file
        if not os.path.exists('./final-output'):
            os.makedirs(to_path)

        o_file='./final-output/' + pdf_file[(pdf_file.rfind('/') + 1) : -4] +'.json'
        output_file = open(o_file, "w")
        json.dump(corpus_json, output_file, indent = 4)

        ##moving pdf file to archived
        move_to_archived(file_name)
        to_path = parsed_yaml_file['paths']['pdf-path']+'archived'
        if not os.path.exists(to_path):
           os.makedirs(to_path)
        shutil.move(pdf_file,to_path)

        ##updating mongo
        collection.delete_one({"pdf-meta.name": meta_name})
        collection.insert_one(corpus_json)
        client.close()
        print("Success")