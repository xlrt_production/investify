import numpy as np
from PIL import Image
import os
import shutil
import pdf2image
import cv2


def get_pdf_images(pdf_images_dir, input_pdf, page_num):
    if os.path.exists(pdf_images_dir):
        shutil.rmtree(pdf_images_dir)
    os.makedirs(pdf_images_dir)
    pdf_name = input_pdf[(input_pdf.rfind('/') + 1) : ]
    try:
        if len(page_num) == 0:
            quit()
        page_num.sort()
        for page in page_num:
            low_dpi_image = pdf2image.convert_from_path(input_pdf, dpi=100, first_page=page, last_page=page,
                                                           fmt='jpg')
            high_dpi_image = pdf2image.convert_from_path(input_pdf, dpi=500, first_page=page, last_page=page,
                                                        fmt='tif')
            temp_path = pdf_images_dir + pdf_name[:-4] + '_Page' + str(page) + 'Temp.jpg'
            file_path = pdf_images_dir + pdf_name[:-4] + '_Page' + str(page) + '.jpg'
            low_dpi_image[0].save(temp_path)
            high_dpi_image[0].save(file_path)
    except Exception:
        print('PDF Error')


def resize_pdf_images(image_dir):
    max_w, max_h = 0, 0
    for img in image_dir:
        high_dpi_image = cv2.imread(img.replace('Temp.jpg', '.jpg'))
        h, w = high_dpi_image.shape[:2]
        if h > max_h or w > max_w:
            max_w, max_h = w, h
    for img in image_dir:
        img1 = img.replace('Temp.jpg', '.jpg')
        high_dpi_image = cv2.imread(img1)
        retina_image = cv2.imread(img)
        high_dpi_image = cv2.resize(high_dpi_image, (max_w, max_h), interpolation=cv2.INTER_AREA)
        retina_image = cv2.resize(retina_image, (max_w, max_h), interpolation=cv2.INTER_AREA)
        cv2.imwrite(img, retina_image)
        cv2.imwrite(img1, high_dpi_image)


def non_max_suppression(boxes, overlapThresh=0.4):
    if len(boxes) == 0:
        return []
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")
    pick = []
    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
 
    while len(idxs) > 0:
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        overlap = (w * h) / area[idxs[:last]]
        idxs = np.delete(idxs, np.concatenate(([last],
            np.where(overlap > overlapThresh)[0])))

    return boxes[pick].astype("int")

# Assign coordinates for rectangle in a dictionary
def gen_rectangle_box(x1, y1, x2, y2):
    rect_coord = dict()
    rect_coord['x1'] = x1
    rect_coord['y1'] = y1
    rect_coord['x2'] = x2
    rect_coord['y2'] = y2
    return rect_coord


# Calculates IOU between two bounding boxes
def get_iou(b_box1, b_box2, mod=False):
    x_left = max(b_box1['x1'], b_box2['x1'])
    y_top = max(b_box1['y1'], b_box2['y1'])
    x_right = min(b_box1['x2'], b_box2['x2'])
    y_bottom = min(b_box1['y2'], b_box2['y2'])
    if x_right < x_left or y_bottom < y_top:
        return 0.0
    intersect_area = (x_right - x_left) * (y_bottom - y_top)
    b_box1_area = (b_box1['x2'] - b_box1['x1']) * (b_box1['y2'] - b_box1['y1'])
    b_box2_area = (b_box2['x2'] - b_box2['x1']) * (b_box2['y2'] - b_box2['y1'])
    if not mod:
        iou = intersect_area / float(b_box1_area + b_box2_area - intersect_area)
    else:
        iou = intersect_area / float(min(b_box1_area, b_box2_area))
    return iou