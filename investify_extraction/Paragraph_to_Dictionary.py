import cv2
import numpy as np
import os, json
import pytesseract
from PIL import Image
import pandas as pd
import yaml
import statistics

##parsing yaml file to fetch the paths

paths_yaml_file = open("./paths.yaml")
parsed_yaml_file = yaml.load(paths_yaml_file, Loader=yaml.FullLoader)

output_dir = parsed_yaml_file['paths']['output_dir']

# # getBound function
# ### It can take 2 arguments, 1st one is image_path which is positional argument & 2nd one is debug which is an optional argument. Function will return all a dictionary with all the paragraphs along with co-ordinates after extracting from the given images.
# #### If debug is true, then function will create a debug folder where all the image processing will store.

def getBound(para_cells,image_path, debug=False):

    '''
        @name  : getBound
        @syntax: getBound(image_path) or getBound(image_path, True)
        @args  : image_path:str
                 debug:boolean
        @return: dict
        @author: aniket@prmfincon.com(Aniket)
    '''
    
    # get filename
    FILE_NAME = image_path.split(".")[1].split("/")[-1]
    print(FILE_NAME)
    paragraph_count = 0
    DEBUG_PATH = './debug/'
    data_content = list()
    linek = np.zeros((11, 11),dtype=np.uint8)
    linek[5,...] = 1
    erode_kernel = np.ones((3, 3), np.uint8)
    dilate_kernel = np.ones((15, 30), np.uint8) 
    
    # create debug folder
    if debug == True:
        try:
            os.mkdir(DEBUG_PATH)
        except FileExistsError:
            pass

    # get image in Color & BW form
    img = cv2.imread(image_path, 1)
    img_2 = cv2.imread(image_path, 1)
    mid_x = img.shape[1]
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    adaptive_thresh = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    vertical_lines = cv2.morphologyEx(adaptive_thresh, cv2.MORPH_OPEN, linek ,iterations=5)
    adaptive_thresh -= vertical_lines
    erode_image = cv2.erode(adaptive_thresh, erode_kernel) 
    dilate_image = cv2.dilate(erode_image, dilate_kernel, iterations = 3) 

    # store erode data
    if debug == True:
        try:
            os.mkdir(DEBUG_PATH + 'dilate')
        except FileExistsError:
            pass
        cv2.imwrite(DEBUG_PATH + 'dilate/'+FILE_NAME+"_dilate.jpg", dilate_image)

    # find conture
    contours, hierarchy = cv2.findContours(dilate_image, 1, 2)

    # height_list=[abs(cell[1]-cell[3]) for cell in para_cells]
    # try:
    #     min_height=min(height_list)
    #     print('min_height',min_height)
    # except Exception:
    #     min_height=0
        
    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)
        if w > (img.shape[1] / 16) and h > 30:
        #if h>= min_height -20 :
            # draw rectangle
            img_2 = cv2.rectangle(img_2,(x,y),(x+w,y+h),(0,255,0),3)
            
            # crop area from image
            cropped_img = img[y:y+h, x:x+w]
            paragraph_count += 1
            
            content = pytesseract.image_to_string(Image.fromarray(cropped_img))
            if content != '':
                data_content.append({"value" : content, "coordinate" : {"x1":x, "y1":y, "x2":x+w, "y2":y+h}})
			
            # put text
            cood_text = "("+str(x)+", "+str(y)+")"
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img_2, cood_text, ((x, y)), font, 1, (0,0,255), 2, cv2.LINE_AA)

            # store cropped data
            if debug == True:
                try:
                    os.mkdir(DEBUG_PATH + '/crop/')
                except FileExistsError:
                    pass

                try:
                    os.mkdir(DEBUG_PATH + 'crop/'+FILE_NAME)
                except FileExistsError:
                    pass

                cv2.imwrite(DEBUG_PATH + 'crop/'+FILE_NAME+"/paragraph_"+str(paragraph_count)+".jpg", cropped_img)

    # store bounding box image
    if debug == True:
        try:
            os.mkdir(DEBUG_PATH + 'bound')
        except FileExistsError:
            pass
        cv2.imwrite(DEBUG_PATH + 'bound/'+FILE_NAME+"_bounded.jpg", img_2)

    return data_content


def get_paragraph(file_name,para_dict):

    ''' @name  : get_paragraph
        @syntax: get_paragraph(folder_path)
        @args  : folder_path:str
        @return: json
        @author: aniket@prmfincon.com(Aniket)
    '''   
    # intialize a variable for final store
    data_dict = {}
    folder_path=output_dir+file_name[(file_name.rfind('/') + 1) : -4]
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            para_cells = []
            for idx in range (len(para_dict['data'])):
                if file == para_dict['data'][idx]['page_name'] : 
                    para_cells= para_dict['data'][idx]['para_cells'] 
                    break

            data_dict[str(file).split(".")[0]] = getBound(para_cells,root+"/"+file, debug=True) 
    return data_dict


# def get_tesseract_output(image_path, para_cells):
#     image = cv2.imread(image_path)
#     data_content=list()

#     height=find_height(para_cells)
#     print(height)

#     for cell in para_cells:
#         img_crop = image[cell[1]:cell[3], cell[0]:cell[2]]
#         try:
#             content = pytesseract.image_to_string(Image.fromarray(img_crop))
#             data_content.append({"value" : content, "coordinate" : {"x1":int(cell[0]), "y1":int(cell[1]), "x2":int(cell[2]), "y2":int(cell[3])}})
#             print(cell[1],content)
            
#         except Exception:
#             print("not identified")
#             continue
#     return data_content


# def get_bound_box(para_cells,image_path):
#     image = cv2.imread(image_path)
#     for cell in para_cells:
#         cood_text = "("+str(cell[0])+", "+str(cell[1])+")"
#         font = cv2.FONT_HERSHEY_SIMPLEX
#         cv2.putText(image, cood_text, ((cell[0],cell[1] )), font, 1, (0,0,255), 2, cv2.LINE_AA)
#         cv2.rectangle(image, (cell[0], cell[1]),
#                                         (cell[2], cell[3]), (0, 255, 0), 2)
#     name=image_path.split('/')[-1]
#     cv2.imwrite("./debug/"+name+".jpg",image)

# def find_height(para_cells):
#     height_list=[abs(cell[1]-cell[3]) for cell in para_cells]
#     median_height=(statistics.median(height_list))
    
#     return median_height